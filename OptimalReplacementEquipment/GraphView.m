//
//  GraphView.m
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import "GraphView.h"
#import "CircleModel.h"
#import "ArrowModel.h"


@interface GraphView()


@property (nonatomic, strong) NSMutableArray *circles;
@property (nonatomic, strong) NSMutableArray *arrows;

@property (nonatomic, assign) NSInteger yearsCount;

@property (nonatomic, assign) CGFloat x0;
@property (nonatomic, assign) CGFloat y0;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGFloat xStep;
@property (nonatomic, assign) CGFloat yStep;



@end

@implementation GraphView

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self recalculateProperties];
}

- (void)awakeFromNib
{
    [self recalculateProperties];
}

- (void)setYears:(NSInteger)years
{
    self.yearsCount = years;
    [self recalculateProperties];
}

- (void)addCircle:(CircleModel *)circle
{
    if (!_circles) {
        _circles = [NSMutableArray array];
    }
    
    [_circles addObject:circle];
    [self setNeedsDisplay];
}

- (void)makeActiveCircle:(CircleModel *)circle
{
    for (CircleModel *myCircle in self.circles) {
        if (circle.k == myCircle.k &&
            circle.t == myCircle.t) {
            myCircle.isGoodWay = YES;
            break;
        }
    }
    [self setNeedsDisplay];
}

- (void)addArrow:(ArrowModel *)arrowModel
{
    if (!_arrows) {
        _arrows = [NSMutableArray array];
    }
    
    [_arrows addObject:arrowModel];
    [self setNeedsDisplay];
}

- (void)setYearsCount:(NSInteger)yearsCount
{
    _yearsCount = yearsCount;
    [self setNeedsDisplay];
}

- (void)recalculateProperties
{
    static const CGFloat offset = 30;
    _x0 = offset;
    _y0 = CGRectGetMaxY(self.bounds) - offset;
    _width = CGRectGetWidth(self.frame) - 2*offset;
    _height = CGRectGetHeight(self.frame) - 2*offset;
    
    _xStep = _width / _yearsCount;
    _yStep = _height / _yearsCount;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
     
     CGContextClearRect(context, rect);
    
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextFillRect(context, rect);
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    
    [self drawAxisInContext:&context];
    [self drawXMarksInContext:&context];
    [self drawYMarksInContext:&context];
    [self drawArrowsInContext:&context];
    [self drawCirclesInContext:&context];
}

- (void)drawAxisInContext:(CGContextRef *)context_
{
    CGContextRef context = *context_;
    
    CGContextMoveToPoint(context, _x0, _y0 - _height);
    CGContextAddLineToPoint(context, _x0, _y0);
    CGContextAddLineToPoint(context, _x0 + _width, _y0);
    
    CGContextStrokePath(context);
    
    NSDictionary *attrs = @{NSFontAttributeName: [UIFont systemFontOfSize:17],
                            NSForegroundColorAttributeName: [UIColor blackColor]};
        
    // draw title
    NSString *title = @"0";
    
    CGSize titleSize = [title sizeWithAttributes:attrs];
    
    CGRect rect = CGRectMake(_x0 - titleSize.width,
                             _y0,
                             titleSize.width,
                             titleSize.height);
    [title drawInRect:rect withAttributes:attrs];
    
}

- (void)drawXMarksInContext:(CGContextRef *)context_
{
    CGContextRef context = *context_;
    
    NSDictionary *attrs = @{NSFontAttributeName: [UIFont systemFontOfSize:17],
                            NSForegroundColorAttributeName: [UIColor blackColor]};
    
    CGFloat stepX = roundf(_width / _yearsCount);
    for (int i = 1; i <= _yearsCount; i++) {
        
        CGFloat x = stepX*i + _x0;
        
        // draw line
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, x, _y0 + 5);
        CGContextAddLineToPoint(context, x, _y0 - 5);
        CGContextStrokePath(context);
        
        
        // draw title
        NSString *title = [NSString stringWithFormat:@"%d",i];
        
        CGPoint anchorPoint = CGPointMake(x
                                           , _y0 + 2);
        CGSize titleSize = [title sizeWithAttributes:attrs];
        
        CGRect rect = CGRectMake(anchorPoint.x - titleSize.width,
                                 anchorPoint.y,
                                 titleSize.width,
                                 titleSize.height);
        [title drawInRect:rect withAttributes:attrs];
        
    }
}

- (void)drawYMarksInContext:(CGContextRef *)context_
{
    CGContextRef context = *context_;
    
    NSDictionary *attrs = @{NSFontAttributeName: [UIFont systemFontOfSize:17],
                            NSForegroundColorAttributeName: [UIColor blackColor]};
    
    CGFloat stepY = roundf(_height / _yearsCount);
    for (int i = 1; i <= _yearsCount; i++) {
        
        CGFloat y = _y0 - stepY*i;
        
        // draw line
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, _x0 + 5, y);
        CGContextAddLineToPoint(context, _x0 - 5, y);
        CGContextStrokePath(context);
        
        // draw title
        NSString *title = [NSString stringWithFormat:@"%d",i];
        
        CGPoint anchorPoint = CGPointMake(_x0 - 5, y);
        CGSize titleSize = [title sizeWithAttributes:attrs];
        
        CGRect rect = CGRectMake(anchorPoint.x - titleSize.width,
                                 anchorPoint.y - titleSize.height / 2,
                                 titleSize.width,
                                 titleSize.height);
        [title drawInRect:rect withAttributes:attrs];
        
    }
}

- (void)drawCirclesInContext:(CGContextRef *)context_
{
    CGContextRef context = *context_;
    
    
    for (CircleModel *circle in self.circles) {
        [self drawCircle:circle inContext:context_];
    }
}

- (void)drawCircle:(CircleModel *)circleModel inContext:(CGContextRef *)context_
{
    CGContextRef context = *context_;
    
    CGPoint center = [self centerForCircle:circleModel];
    CGFloat radius = [self circleRadius];
    
    CGRect circleRect = CGRectMake(center.x - radius,
                                   center.y - radius,
                                   radius*2,
                                   radius*2);
    
    CGContextSetFillColorWithColor(context, [circleModel color].CGColor);
    CGContextFillEllipseInRect(context, circleRect);
    
    // draw title
    NSString *title = [NSString stringWithFormat:@"%.0f",circleModel.coasts];
    NSDictionary *attrs = @{NSFontAttributeName: [UIFont systemFontOfSize:10],
                            NSForegroundColorAttributeName: [UIColor blackColor]};
    
    CGSize titleSize = [title sizeWithAttributes:attrs];
    
    CGRect rect = CGRectMake(center.x - titleSize.width / 2,
                             center.y - titleSize.height / 2,
                             titleSize.width,
                             titleSize.height);
    [title drawInRect:rect withAttributes:attrs];
}

- (void)drawArrowsInContext:(CGContextRef *)context_
{
    for (ArrowModel *arrow in _arrows) {
        [self drawArrow:arrow inContext:context_];
    }
}

- (void)drawArrow:(ArrowModel *)arrow inContext:(CGContextRef *)context_
{
    CGContextRef context = *context_;
    
    CGPoint start,end;
    
    CGContextSetLineWidth(context, 2);
    CGContextSetStrokeColorWithColor(context, [arrow color].CGColor);
    
    [self arrowStartPoint:&start andEndPoint:&end forArrow:arrow];
    
    CGContextMoveToPoint(context, start.x, start.y);
    CGContextAddLineToPoint(context, end.x, end.y);
    
    CGContextStrokePath(context);    
}

#pragma mark - Helpers

- (CGFloat)circleRadius
{
    CGFloat minSize = MIN(_xStep, _yStep);
   
    CGFloat radius = minSize / 2 - 6;
    
    return radius;
}

- (CGPoint)centerForCircle:(CircleModel *)circle
{
    return [self centerForK:circle.k andT:circle.t];
}

- (CGPoint)centerForK:(NSInteger)k andT:(NSInteger)t
{
    CGFloat x = _x0 + k * _xStep;
    CGFloat y = _y0 - t * _yStep;
    
    CGPoint center = CGPointMake(x, y);
    
    return center;
}

- (void)arrowStartPoint:(CGPoint *)startPoint andEndPoint:(CGPoint *)endPoint forArrow:(ArrowModel *)arrow
{
    CGPoint circeStartPoint = [self centerForK:arrow.kFrom andT:arrow.tFrom];
    CGPoint circeEndPoint = [self centerForK:arrow.kTo andT:arrow.tTo];
    
    CGFloat w = fabsf(circeEndPoint.x - circeStartPoint.x);
    CGFloat h = fabsf(circeEndPoint.y - circeStartPoint.y);
    
    CGFloat angle = atanf(w/h); // angle from start point to end point RAD
    
    CGFloat radius = [self circleRadius] + 2;
    
    CGFloat startX,startY,endX,endY;
    
    if (circeStartPoint.y >= circeEndPoint.y) {
        startX = circeStartPoint.x + radius * sin(angle);
        startY = circeStartPoint.y - radius * cos(angle);
        
        endX = circeEndPoint.x - radius * sin(angle);
        endY = circeEndPoint.y + radius * cos(angle);
    } else {
        startX = circeStartPoint.x + radius * sin(angle);
        startY = circeStartPoint.y + radius * cos(angle);
        
        endX = circeEndPoint.x - radius * sin(angle);
        endY = circeEndPoint.y - radius * cos(angle);
        
    }
    
    
    *startPoint = CGPointMake(startX, startY);
    *endPoint = CGPointMake(endX, endY);
}

@end
