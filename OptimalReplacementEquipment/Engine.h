//
//  Engine.h
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CacheManager.h"
#import "CalculateManager.h"

@interface Engine : NSObject

+ (instancetype)sharedInstance;
- (CacheManager *)cacheManager;
- (CalculateManager *)calculateManager;

@end
