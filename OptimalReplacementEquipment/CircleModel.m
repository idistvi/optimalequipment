//
//  CircleModel.m
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import "CircleModel.h"

@implementation CircleModel

- (UIColor *)color
{
    UIColor *grayColor = [UIColor grayColor];
    UIColor *greenColor = [UIColor greenColor];
    
    UIColor *resultColor = self.isGoodWay ? greenColor : grayColor;
    
    return resultColor;
}

@end
