//
//  CircleModel.h
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleModel : NSObject

@property (nonatomic, assign) NSInteger k; // step
@property (nonatomic, assign) NSInteger t; // yearsInExploitation
@property (nonatomic, assign) CGFloat coasts;

@property (nonatomic, weak) CircleModel *nextGoodState;

@property (nonatomic, assign) BOOL isGoodWay;
@property (nonatomic, assign) BOOL isNextStepSupport;

- (UIColor *)color;

@end
