//
//  Engine.m
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import "Engine.h"

@interface Engine()

@property (nonatomic, strong) CacheManager *cacheManager;
@property (nonatomic, strong) CalculateManager *calculateManager;

@end

@implementation Engine

+ (instancetype)sharedInstance
{
    static Engine *singleton = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[Engine alloc] init];
    });
    
    return singleton;
}

- (CacheManager *)cacheManager
{
    if (!_cacheManager) {
        _cacheManager = [[CacheManager alloc] init];
    }
    return _cacheManager;    
}

- (CalculateManager *)calculateManager
{
    if (!_calculateManager) {
        _calculateManager = [[CalculateManager alloc] init];
    }
    return _calculateManager;
}

@end
