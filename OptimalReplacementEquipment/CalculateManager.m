//
//  CalculateManager.m
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import "CalculateManager.h"

@implementation CalculateManager

- (CGFloat)saleForStartPrice:(CGFloat)price afterYears:(NSInteger)years
{
    CGFloat sale = price * powf(2, -years);
    return sale;
}

- (CGFloat)supportPriceWithStartPrice:(CGFloat)supportStartPrice afterYears:(NSInteger)years
{
    // r0 * (t + 1)
    CGFloat supportPrice = supportStartPrice * (years + 1);
    return supportPrice;
}



@end
