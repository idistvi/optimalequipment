//
//  GraphViewController.m
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import "GraphViewController.h"
#import "GraphView.h"
#import "Engine.h"
#import "CircleModel.h"
#import "ArrowModel.h"
#import "CacheManager.h"
#import "CalculateManager.h"
#import "ResultViewController.h"

@interface GraphViewController ()

@property (strong, nonatomic) IBOutlet GraphView *graphView;
@property (weak, nonatomic) IBOutlet UIButton *autoRunButton;
@property (weak, nonatomic) IBOutlet UIButton *resultButton;

@property (nonatomic, assign) NSInteger currentK;
@property (nonatomic, assign) NSInteger currentT;

@property (nonatomic, strong) NSMutableArray *circleMatrix;

@property (nonatomic, strong) NSTimer *autoTimer;

@property (nonatomic, strong) NSMutableArray *resultStrings;

@end

@implementation GraphViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSInteger yearsCount = [[[Engine sharedInstance] cacheManager] yearsOfSupport];
    
    self.graphView.layer.borderWidth = 1;
    self.graphView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    [self.graphView setYears:yearsCount];
    
    
    _circleMatrix = [NSMutableArray array];
    
    for (int k = 0; k <= yearsCount; k++) {
        NSMutableArray *tArray = [NSMutableArray array];
        for (int t = 0; t <= k; t++) {
            CircleModel *circle = [[CircleModel alloc] init];
            circle.t = t;
            circle.k = k;
            
            [tArray addObject:circle];
        }
        [_circleMatrix addObject:tArray];
    }
    
    _currentK = yearsCount;
    _currentT = 1;
    
    _resultButton.hidden = YES;
}

#pragma mark - Algo

- (BOOL)nextStep
{
    if (_currentT == 0) {
        return NO;
    }
    
    if (++_currentT > _currentK) {
        _currentK--;
        _currentT = _currentK == 0 ? 0 : 1;
        
    }
    
    return YES;
}

- (void)calculateAndDrawObjects
{
    CGFloat price0 = [[[Engine sharedInstance] cacheManager] productPrice];
    CGFloat support0 = [[[Engine sharedInstance] cacheManager] productSupportPrice];
    NSInteger years = [[[Engine sharedInstance] cacheManager] yearsOfSupport];
    
    NSInteger k = _currentK;
    NSInteger t = _currentT;
    
    CGFloat supportCoast = [[[Engine sharedInstance] calculateManager] supportPriceWithStartPrice:support0 afterYears:t];
    CGFloat saleProductCoast = [[[Engine sharedInstance] calculateManager] saleForStartPrice:price0 afterYears:t];
    
    CircleModel *circle = _circleMatrix[k][t];
    
    if (k == 0) {
        CircleModel *nextCircle = _circleMatrix[k+1][t+1];
        CGFloat nextStateCoast = nextCircle.coasts;
        circle.coasts = nextStateCoast + support0 + price0;
        circle.nextGoodState = nextCircle;
        circle.isNextStepSupport = NO;
        
        ArrowModel *arrow = [[ArrowModel alloc] init];
        arrow.kFrom = k;
        arrow.tFrom = t;
        arrow.kTo = k+1;
        arrow.tTo = t+1;
        arrow.isGoodWay = YES;
        [self.graphView addArrow:arrow];
        
        
    } else if (k < years) {
        CircleModel *nextCircle1 = _circleMatrix[k+1][t+1];
        CGFloat nextState1Coast = nextCircle1.coasts;
        
        CircleModel *nextCircle2 = _circleMatrix[k+1][1];
        CGFloat nextState2Coast = nextCircle2.coasts;
        
        CGFloat way1Coast = supportCoast + nextState1Coast ;
        CGFloat way2Coast = price0 + support0 - saleProductCoast + nextState2Coast;
        
        BOOL isFirstWayBetter = (way1Coast < way2Coast);
        
        ArrowModel *arrow = [[ArrowModel alloc] init];
        arrow.kFrom = k;
        arrow.tFrom = t;
        arrow.kTo = k+1;
        arrow.tTo = t+1;
        arrow.isGoodWay = isFirstWayBetter;
        [self.graphView addArrow:arrow];
        
        ArrowModel *arrow2 = [[ArrowModel alloc] init];
        arrow2.kFrom = k;
        arrow2.tFrom = t;
        arrow2.kTo = k + 1;
        arrow2.tTo = 1;
        arrow2.isGoodWay = !isFirstWayBetter;
        [self.graphView addArrow:arrow2];
        
        
        circle.coasts = isFirstWayBetter ? way1Coast : way2Coast;
        circle.nextGoodState = isFirstWayBetter ? nextCircle1 : nextCircle2;
        circle.isNextStepSupport = isFirstWayBetter;
        [self.graphView addCircle:circle];
        
    } else {
        circle.coasts = -saleProductCoast;
    }
    
    [self.graphView addCircle:circle];
}

- (void)hightLightBestWay
{
    CircleModel *rootCircle = _circleMatrix[0][0];
    CircleModel *nextCircle = rootCircle;
    
    if (!_resultStrings) {
        _resultStrings = [NSMutableArray array];
    } else {
        [_resultStrings removeAllObjects];
    }
    
    [_resultStrings addObject:[self messageForCircle:nextCircle]];
    
    [self.graphView makeActiveCircle:nextCircle];
    while (nextCircle.nextGoodState) {
        nextCircle = nextCircle.nextGoodState;
        [self.graphView makeActiveCircle:nextCircle];
        [_resultStrings addObject:[self messageForCircle:nextCircle]];
    }
    
    _resultButton.hidden = NO;
}

- (NSString *)messageForCircle:(CircleModel *)circleModel
{
    NSString *deviceName = [[[Engine sharedInstance] cacheManager] projectName];
    CGFloat price0 = [[[Engine sharedInstance] cacheManager] productPrice];
    CGFloat support0 = [[[Engine sharedInstance] cacheManager] productSupportPrice];
    NSInteger years = [[[Engine sharedInstance] cacheManager] yearsOfSupport];
    
    
    if (circleModel.k == 0) {
        return [NSString stringWithFormat:@"Купуємо '%@' та підтримуемо протягом року",deviceName];
    } else if (circleModel.k < years) {
        if (circleModel.isNextStepSupport) {
            CGFloat supportCoast = [[[Engine sharedInstance] calculateManager] supportPriceWithStartPrice:support0 afterYears:circleModel.t];
            return [NSString stringWithFormat:@"Витрати на утримання %.2f",-supportCoast];
        } else {
            CGFloat saleProductCoast = [[[Engine sharedInstance] calculateManager] saleForStartPrice:price0 afterYears:circleModel.t];
            return [NSString stringWithFormat:@"Продаємо за %.2f , та купуємо новий прилад за %.2f",saleProductCoast,price0];
        }
    } else {
        return [NSString stringWithFormat:@"Продаємо '%@' за %.0f",deviceName, fabsf(circleModel.coasts)];
    }
    
    return @"";
}

#pragma mark - IBAction

- (IBAction)nextAction:(id)sender {
    // test method
    
    [self calculateAndDrawObjects];
    if (![self nextStep]) {
        [self hightLightBestWay];
    }
}

- (IBAction)autoAction:(id)sender {
    
    if (self.autoTimer) {
        [self.autoTimer invalidate];
        self.autoTimer = nil;
        [_autoRunButton setTitle:@"Авто" forState:UIControlStateNormal];
    } else {
        self.autoTimer = [NSTimer scheduledTimerWithTimeInterval:0.125 target:self selector:@selector(autoCalculate:) userInfo:nil repeats:YES];
        [_autoRunButton setTitle:@"Стоп" forState:UIControlStateNormal];
    }
    
}

- (void)autoCalculate:(NSTimer *)timer
{
    [self calculateAndDrawObjects];
    if (![self nextStep]) {
        [self.autoTimer invalidate];
        self.autoTimer = nil;
        _autoRunButton.hidden = YES;
        [self hightLightBestWay];
    }
}

- (IBAction)resultAction:(id)sender {
    [self performSegueWithIdentifier:@"DetailSegue" sender:nil];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    ResultViewController *resultVC = [segue destinationViewController];
    
    resultVC.results = _resultStrings;
}


@end
