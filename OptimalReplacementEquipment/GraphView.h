//
//  GraphView.h
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CircleModel;
@class ArrowModel;

@interface GraphView : UIView

- (void)setYears:(NSInteger)years;

- (void)addCircle:(CircleModel *)circle;
- (void)makeActiveCircle:(CircleModel *)circle;
- (void)addArrow:(ArrowModel *)arrowModel;


@end
