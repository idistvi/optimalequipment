//
//  ResultViewController.h
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewController : UIViewController

@property (nonatomic, strong) NSArray *results;

@end
