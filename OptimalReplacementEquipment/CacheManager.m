//
//  CacheManager.m
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import "CacheManager.h"

@implementation CacheManager

- (void)clearData
{
    self.projectName = nil;
    self.productPrice = 0;
    self.productSupportPrice = 0;
    self.yearsOfSupport = 0;
}

@end
