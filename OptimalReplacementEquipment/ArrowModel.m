//
//  ArrowModel.m
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import "ArrowModel.h"

@implementation ArrowModel

- (UIColor *)color
{
    UIColor *grayColor = [UIColor grayColor];
    UIColor *greenColor = [UIColor colorWithRed:0 green:0.5 blue:0 alpha:1];
    
    UIColor *resultColor = self.isGoodWay ? greenColor : grayColor;
    
    return resultColor;
}

@end
