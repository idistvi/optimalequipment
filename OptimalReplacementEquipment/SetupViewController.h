//
//  SetupViewController.h
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <LHSKeyboardAdjusting/LHSKeyboardAdjusting.h>

@interface SetupViewController : UIViewController <LHSKeyboardAdjusting>

@end
