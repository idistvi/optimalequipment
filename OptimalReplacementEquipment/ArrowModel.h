//
//  ArrowModel.h
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArrowModel : NSObject

@property (nonatomic, assign) NSInteger kFrom; // stepFrom
@property (nonatomic, assign) NSInteger tFrom; // yearsInExploitationFrom

@property (nonatomic, assign) NSInteger kTo; // stepTo
@property (nonatomic, assign) NSInteger tTo; // yearsInExploitationTo

@property (nonatomic, assign) CGFloat coasts;

@property (nonatomic, assign) BOOL isGoodWay;

- (UIColor *)color;

@end
