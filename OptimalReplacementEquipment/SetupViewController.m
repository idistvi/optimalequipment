//
//  SetupViewController.m
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import "SetupViewController.h"
#import <LHSKeyboardAdjusting/UIViewController+LHSKeyboardAdjustment.h>

#import "Engine.h"

@interface SetupViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *EquipmentName;
@property (weak, nonatomic) IBOutlet UITextField *priceTF;
@property (weak, nonatomic) IBOutlet UITextField *supportProceTF;
@property (weak, nonatomic) IBOutlet UITextField *yearsTF;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@end

@implementation SetupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[[Engine sharedInstance] cacheManager] clearData];
    
    self.bottomConstraint = [NSLayoutConstraint constraintWithItem:self.scrollView
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.bottomLayoutGuide
                                                         attribute:NSLayoutAttributeTop
                                                        multiplier:1
                                                          constant:0];
    [self.view addConstraint:self.bottomConstraint];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self lhs_activateKeyboardAdjustment];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self lhs_deactivateKeyboardAdjustment];
}

#pragma mark - IBActions

- (IBAction)nextAction:(UIButton *)sender {
    [[[Engine sharedInstance] cacheManager] setProjectName:self.EquipmentName.text];
    [[[Engine sharedInstance] cacheManager] setProductPrice:[self.priceTF.text integerValue]];
    [[[Engine sharedInstance] cacheManager] setProductSupportPrice:[self.supportProceTF.text integerValue]];
    [[[Engine sharedInstance] cacheManager] setYearsOfSupport:[self.yearsTF.text integerValue]];
    
    [self performSegueWithIdentifier:@"CalculateSegue" sender:nil];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - LHSKeyboardAdjusting

- (NSLayoutConstraint *)keyboardAdjustingBottomConstraint {
    return self.bottomConstraint;
}

#pragma mark - Private methods

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
