//
//  CalculateManager.h
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculateManager : NSObject

- (CGFloat)saleForStartPrice:(CGFloat)price afterYears:(NSInteger)years;
- (CGFloat)supportPriceWithStartPrice:(CGFloat)supportStartPrice afterYears:(NSInteger)years;

@end
