//
//  CacheManager.h
//  OptimalReplacementEquipment
//
//  Created by Stas on 04.03.15.
//  Copyright (c) 2015 Distvi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheManager : NSObject

@property (nonatomic, strong) NSString *projectName;
@property (nonatomic, assign) NSInteger productPrice;
@property (nonatomic, assign) NSInteger productSupportPrice;
@property (nonatomic, assign) NSInteger yearsOfSupport;

- (void)clearData;

@end
